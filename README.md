This is a project to test out various application technology stacks.

Inspired by [TodoMVC](http://todomvc.com/).

Switch between various branches in this repository to see the implementation.

---

# Knockout

This is a web app implementation using [knockout js](http://knockoutjs.com/)

Features:

1. Self-contained reusable components w/ knockout
2. Templates using JSX (TSX)
3. CSS processing w/ webpack
4. Written in Typescript (w/ source maps for easy debugging)
5. Tests written in Typescript, mocha & chai
6. Tests debuggable in browser with reload-on-change
7. Inline styles - in progress

TODO:

* hot reloading (see [ko-hot-loader](https://github.com/Profiscience/ko-hot-loader))
* add tslint
* buildable components

---

# Setup

Install the project dependencies and libraries, then run the project

```
npm install
npm run debug
```

Navigate to [http://localhost:8080](http://localhost:8080)

# Testing

Running tests

```
npm run test
```

Debugging tests:

```
npm run test:debug
```

Navigate to [http://localhost:8080/spec.html](http://localhost:8080/spec.html)

# Notes

Other libraries to use:

* [ES6 promise](https://github.com/stefanpenner/es6-promise)
* [Reqwest](https://github.com/ded/Reqwest)
* [Request](https://github.com/request/request)
* [Page](https://github.com/visionmedia/page.js)
* [ko-component-router](https://profiscience.github.io/ko-component-router/#!/)