import { createTodo, Todo } from '../../src/models/todo';
import { expect } from 'chai';

describe('Todo model', () => {
    let todo: Todo;
    
    beforeEach(() => {
        todo = createTodo('newTodo');
    });
    
    it('has title set when created', () => {
        expect(todo.title()).to.equal('newTodo');
    });
    
    it('is not completed by default', () => {
        expect(todo.completed()).to.be.false;
    });
    
    it('is not edited by default', () => {
        expect(todo.editing()).to.be.false;
    });
    
    it('has empty old title', () => {
        expect(todo.oldTitle).to.be.empty;
    });
});