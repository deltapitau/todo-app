import * as appModel from '../../src/models/app';
import { expect } from 'chai';

describe('Application Model', () => {
    let app: appModel.App;
    
    beforeEach(() => {
        app = appModel.createApp();
    });
    
    it('has empty new todo by default', () => {
        expect(app.current()).to.be.empty;
    });
    
    it('has no todos by default', () => {
        expect(app.todos.length).to.equal(0);
    });
    
    it('shows all todos by default', () => {
        expect(app.show()).to.equal(appModel.VisibleTodos.All);
    });
});