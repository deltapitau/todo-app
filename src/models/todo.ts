import * as ko from 'knockout';

export interface Todo {
    title: KnockoutObservable<string>;
    completed: KnockoutObservable<boolean>;
    editing: KnockoutObservable<boolean>;
    oldTitle: string;
}

export function createTodo(title: string): Todo {
    return {
        title: ko.observable<string>(title),
        completed: ko.observable<boolean>(false),
        editing: ko.observable<boolean>(false),
        oldTitle: ''
    };
};