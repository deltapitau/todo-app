import * as ko from 'knockout';
import { Todo } from './todo';

export const enum VisibleTodos {
    All,
    Active,
    Completed
}

export interface App {
    current: KnockoutObservable<string>;
    todos: KnockoutObservableArray<Todo>;
    show: KnockoutObservable<VisibleTodos>;
}

export function createApp(): App {    
    return {
        current: ko.observable<string>(),
        todos: ko.observableArray<Todo>(),
        show: ko.observable(VisibleTodos.All)
    };
};
