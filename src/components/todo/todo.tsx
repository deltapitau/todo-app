import * as ko from 'knockout';
import { tsjsx } from '../../extensions/tsjsx';
import { Todo } from '../../models/todo';

const todoVm = (params: {
        todo: Todo,
        removeCallback: Function
    }) => {

    const todo = params.todo;

    const editItem = () => {
        todo.editing(true);
        todo.oldTitle = todo.title();
    };

    const stopEditing = () => {
        todo.editing(false);

        const title = todo.title();
        const trimmedTitle = title.trim();

        if (title !== trimmedTitle) {
            todo.title(trimmedTitle);
        }

        if (!trimmedTitle) {
            remove();
        }
    };

    const cancelEditing = () => {
        todo.editing(false);
        todo.title(todo.oldTitle);
    };
        
    const remove = () => {
        params.removeCallback(todo);
    };

    return {
        todo: todo,
        editItem: editItem,
        stopEditing: stopEditing,
        cancelEditing: cancelEditing,
        remove: remove
    };
};

const todoTemplate =
<div>
    <div class="view">
        <input class="toggle" data-bind="checked: todo.completed" type="checkbox" />
        <label data-bind={`
            text: todo.title,
            event: { dblclick: editItem }`}></label>
        <button class="destroy" data-bind="click: remove"></button>
    </div>
    <input class="edit"
        data-bind={`
            value: todo.title,
            valueUpdate: 'afterkeydown',
            enterKey: stopEditing,
            escapeKey: cancelEditing,
            selectAndFocus: editing,
            event: { blur: stopEditing }`} />
</div>;

export const registerTodo = () => {
    ko.components.register('todo-view', {
        viewModel: todoVm,
        template: todoTemplate
    });
};
