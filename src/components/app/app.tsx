import * as ko from 'knockout';
import { tsjsx } from '../../extensions/tsjsx';
import { Todo, createTodo } from '../../models/todo';
import { App } from '../../models/app';
import { registerTodo } from '../todo/todo';

const appVm = (params: {
        app: App
    }) => {

    const app = params.app;

    const add = () => {
        let input = app.current().trim();

        if (input) {
            app.todos.push(createTodo(input));
            app.current('');
        }
    };

    const remove = (todo: Todo) => {
        app.todos.remove(todo);
    };

    const removeCompleted = () => {
        app.todos.remove((todo) => {
            return todo.completed();
        });
    };

    const completedCount = ko.computed(() => {
        return ko.utils.arrayFilter(app.todos(), (todo) => {
            return todo.completed();
        }).length;
    });

    const remainingCount = ko.computed(() => {
        return app.todos().length - completedCount();
    });

    const allCompleted = ko.computed({
        read: () => {
            return !remainingCount();
        },
        write: (newValue) => {
            ko.utils.arrayForEach(app.todos(), (todo) => {
                todo.completed(newValue);
            });
        }
    });

    const getLabel = (count: number) => {
        return ko.utils.unwrapObservable(count) === 1 ? 'item' : 'items';
    };

    return {
        todos: app.todos,
        current: app.current,
        add: add,
        remove: remove,
        removeCompleted: removeCompleted,
        completedCount: completedCount,
        remainingCount: remainingCount,
        allCompleted: allCompleted,
        getLabel: getLabel
    };
};

const appTemplate =
<section id="todoapp">
    <header id="header">
        <h1>todos</h1>
        <input id="new-todo"
            data-bind={`
                value: current,
                valueUpdate: 'afterkeydown',
                enterKey: add`}
            placeholder="What needs to be done?" autofocus />
    </header>
    <section id="main" data-bind="visible: todos().length">
        <input id="toggle-all" data-bind="checked: allCompleted" type="checkbox" />
        <label for="toggle-all">Mark all as complete</label>
        <ul id="todo-list" data-bind="foreach: todos">
            <li data-bind="css: { completed: completed, editing: editing }">
                <todo-view params="todo: $data, removeCallback: $parent.remove" />
            </li>
        </ul>
    </section>
    <footer id="footer" data-bind="visible: completedCount() || remainingCount()">
        <span id="todo-count">
            <strong data-bind="text: remainingCount">0</strong>
            <span data-bind="text: getLabel(remainingCount)"></span> left
        </span>
        <button id="clear-completed"
            data-bind={`
                visible: completedCount,
                click: removeCompleted`}>
            Clear completed</button>
    </footer>
</section>;

export const registerApp = () => {
    registerTodo();
    ko.components.register('todo-app', {
        viewModel: appVm,
        template: appTemplate
    });
};