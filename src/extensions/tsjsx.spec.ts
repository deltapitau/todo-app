import { tsjsx } from './tsjsx';
import { expect } from 'chai';

describe('JSX Factory', () => {

    describe('create element', () => {
        it('creates markup for simple element tag', () => {
            let result = tsjsx.createElement('test');
            expect(result).to.equal('<test></test>');
        });

        it('creates markup for element tag with attributes', () => {
            let result = tsjsx.createElement('test', {
                'data-bind': 'binding: value',
                'class': 'className'
            });
            expect(result).to.equal('<test data-bind="binding: value" class="className"></test>');
        });

        it('creates markup for element tag with content', () => {
            let result = tsjsx.createElement('test', undefined, 'content');
            expect(result).to.equal('<test>content</test>');
        });

        it('creates markup for multiple children', () => {
            let result = tsjsx.createElement('a', undefined,
                tsjsx.createElement('b'),
                tsjsx.createElement('c'));
            expect(result).to.equal('<a><b></b><c></c></a>');
        });

        it('creates markup for nested elements with attributes and content', () => {
            let result = tsjsx.createElement('a', {
                'class': 'className',
                'aria-label': 'label'
            }, tsjsx.createElement('b', undefined, 'content'));
            expect(result).to.equal('<a class="className" aria-label="label"><b>content</b></a>');
        });

        it('removes unneccessary whitespace from the markup', () => {
            let result = tsjsx.createElement('test', {
                'class': 'some \
                        name',
                'data-bind': 'first      second'
            });
            expect(result).to.equal('<test class="some name" data-bind="first second"></test>');
        });
    });
});