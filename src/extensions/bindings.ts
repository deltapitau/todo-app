import * as ko from 'knockout';

const ENTER_KEY = 13,
      ESCAPE_KEY = 27;

function createKeyBindingFor(key: number): KnockoutBindingHandler {
    return {
        init: (element, valueAccessor, allBindingsAccessor, data, bindingContext) => {
            let wrappedHandler = (data: any, event: any) => {
                    if (event.keyCode === key) {
                        valueAccessor().call(this, data, event);
                    }
                },
                newValueAccessor = () => {
                    return {
                        keyup: wrappedHandler
                    };
                };
            
            ko.bindingHandlers.event.init(element, newValueAccessor, allBindingsAccessor, data, bindingContext);
        }
    };
}

export function registerKeyBindings() {
    (<any>ko.bindingHandlers).enterKey = createKeyBindingFor(ENTER_KEY);
    (<any>ko.bindingHandlers).escapeKey = createKeyBindingFor(ESCAPE_KEY);
}

export function registerBindings() {
    registerKeyBindings();
}