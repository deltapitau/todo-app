function toStringArray(object: any, toString: (key: string, value?: string) => string ): string[] {
    let array: string[] = [];
    
    for (let name in object) {
        if (object.hasOwnProperty(name)) {
            array.push(toString(name, object[name]));
        }
    }

    return array;
}

function createElement(tagName: string, attributes?: any, ...children: Array<string>): string {
    let attrMarkup = toStringArray(attributes, (key, value) => {
        return `${key}="${value}"`;
    }).join(' ');

    let openTag = !attrMarkup ? tagName : `${tagName} ${attrMarkup}`;
    let contentMarkup = children.join('') + '';

    return `<${openTag}>${contentMarkup}</${tagName}>`.replace(/\s+/g, ' ').trim();
}

export let tsjsx = {
    createElement: createElement
};
