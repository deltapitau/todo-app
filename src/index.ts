import * as ko from 'knockout';
import { createApp } from './models/app';
import { registerApp } from './components/app/app';
import { registerBindings } from './extensions/bindings';

registerBindings();
registerApp();

ko.applyBindings(createApp());