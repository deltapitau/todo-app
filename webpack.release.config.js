module.exports = {
    entry: './src/index',
    resolve: {
        extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js'],
        modules: [
            __dirname,
            "node_modules"
        ],
        alias: {
            knockout: 'node_modules/knockout/build/output/knockout-latest.js'
        }
    },
    module: {
        rules: [{
            test: /\.tsx?$/,
            use: {
                loader: 'ts-loader'
            }
        }]
    },
    output: {
        path: __dirname + '/dist',
        publicPath: '/dist',
        filename: 'bundle.min.js'
    }
};